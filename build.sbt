name := "crypto"
organization := "com.sludg.util"
version := "0.1"

scalaVersion := "2.13.0"
crossScalaVersions := Seq("2.12.10", "2.11.12")

resolvers += "SLUDG@GitLab" at "https://gitlab.com/api/v4/groups/SLUDG/-/packages/maven"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test

enablePlugins(GitlabPlugin)
com.sludg.sbt.gitlab.GitlabPlugin.gitlabProjectId := "14283903"